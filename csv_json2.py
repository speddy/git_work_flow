'''Takes CSV data and converts to a JSON output ''' 
import json
import numpy as np

def returnPythonDic(labresults):
    '''Takes lab results data and puts them in with patient data'''
    patients = [{} for i in range(len(hospUnique))]
    for pat in range(len(hospUnique)):
    
        dicto =patients[pat]
        getData= patientData[pat]
        dicto["id"] = getData["id"] 
        dicto["firstName"]=getData["firstName"]
        dicto["lastName"]=getData["lastName"]
        dicto["dob"]=getData["dateOfBirth"]
        dicto["lab_results"] =labresults[pat]
    
    return patients
    
def getLabresults():
    '''First convert data into python dictionary form '''
    
    labresults=[[] for i in range(len(hospUnique))] # each patient has a seperate group of panels
    PanelHeader = [[] for j in range(len(hospUnique))] # will keep a record of groups of panels. 
    # Panel "headers" correspond to a (timestamp, profile)
    
    for line in range(1,len(CSVlabresults)):
        splitter = CSVlabresults[line].split(",")
        patientIndex=hospUniq.index(splitter[0]) #  which patient is the line data for?
        PatientPanelHeader=PanelHeader[patientIndex]
        Patientlabresults=labresults[patientIndex]
        
        if '"' in splitter[3]:
            # Split works fine except in the case of double quotation marks " ," 
            removedElement = splitter.pop(4)
            splitter[3] = splitter[3] +","+ removedElement
        
        if (splitter[2]+" "+splitter[3]+" "+splitter[4]) not in PatientPanelHeader:
            # initialise a new dictionary entry for the list lab results (specific to that patient)
            PatientPanelHeader.append(splitter[2]+" "+splitter[3]+" "+splitter[4])
            # set up a panel list, and fill in the first entry
            y, = np.where(Keys==splitter[-4])[0] # match code in codes text file
            
            panelList=[{"code":Codes[y],"label":Descriptions[y],
            "value": (splitter[5].split("~")[1]),
            "unit":splitter[-3],
            "lower":splitter[-2],
            "upper":splitter[-1].replace("\r\n","") # get rid of newline marker
            }]
            lab_dic_entry={"timestamp":splitter[2],
            "profile":{"name":splitter[3],"code":splitter[4]},
            "panel":panelList
            }
            Patientlabresults.append(lab_dic_entry)
            
        else:
            # A panel header already exists for this patient and specific panel
            position=PatientPanelHeader.index((splitter[2]+" "+splitter[3]+" "+splitter[4])) 
            # where abouts in panel header is it found
            currentDic_entry=Patientlabresults[position] # we already have an entry with this panel header
            currentPanelList=currentDic_entry["panel"]
            y, = np.where(Keys==splitter[-4])[0] # match code in codes text file
            test = splitter[-4]
            # need to check through Res1--->Res25 to get the right result
            possible = splitter[5:-4]
            valueOfInterest=[s for s in possible if test in s] # does it contain the test keyword?
            # Values are formatted 'example: ALP~55' with the test followed by the result
            valueTrimmed= (valueOfInterest)[0].split("~")[1]
            
            NewPanel = {"code":Codes[y],"label":Descriptions[y],
            "value": valueTrimmed, # will not actually be this value
            "unit":splitter[-3],
            "lower":splitter[-2],
            "upper":splitter[-1].replace("\r\n","")# get rid of newline marker
                }
            currentPanelList.append(NewPanel) # add the panel to the list of panels already in the entry
            currentDic_entry["panel"]=currentPanelList # update the dictionary
        
    return labresults
        
def PythonToJson(patientDictionary):
    '''Takes python dictionary and write to JSON file '''
    with open('output.json', 'w') as fp:
        json.dump(patientDictionary, fp, indent=4) # pretty print save

with open('patients.json','r') as f:
    patientData=json.load(f) # Json patient data
    
with open('labresults.csv','rb') as f1:
    CSVlabresults = f1.readlines() # lab results text file 
hospID= np.genfromtxt('labresults.csv',dtype=str,delimiter=",",skip_header=1,
    unpack=True,usecols=0)
hospUnique = np.unique(hospID) 
hospUniq=hospUnique.tolist() # so we can use .index on it


Keys,Codes = np.genfromtxt('labresults-codes.csv',dtype=str,delimiter=",",
    skip_header=1,unpack=True,usecols=(0,1)) # read the data from codes file
# Description data is not as easily accesed using np.genfromtxt
with open('labresults-codes.csv','rb') as f2:
    DescriptionData = f2.readlines()
Descriptions=[]
for des in range(1,len(DescriptionData)):
    neater =(",".join(DescriptionData[des].split(',')[2:]))[:-1]
    neater=neater.rstrip()
    neater=neater.replace('"','')
    Descriptions.append(neater)



LabResults = getLabresults()
patientDictionary = {"patients":returnPythonDic(LabResults)}
PythonToJson(patientDictionary)

# If wanting to view the JSON string 
# print json.dumps(patientDictionary, indent=4)
